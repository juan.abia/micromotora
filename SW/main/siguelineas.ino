// CONSTANTES
const int kp = 30;                // Proporcional
const int kd = 5;                 // Derivativo
const int intervalo_calibrar = 3; // Numero de veces que se realiza el PID antes de calibrar el siguelineas

// VARIABLES
int   u = 0;
float p = 0;
int   d = 0;
int previous_p = 0;         
float vbase = 150;            // Potencia base (0-255)
int count_calibrate = 0;      // numero de veces ejecutado el algoritmo antes de calibrar
int l_speed, r_speed;         // Potencia de cada rueda
int average_intensity = 890;  // Intensidad media del sensor QTR-8A 
int max_intensity = 0;        // Intensidad maxima    
int min_intensity = 4000;     // Intensidad minima
int matriz[8] = {13, 14, 27, 26, 25, 33, 32, 35};   // Matriz con los pines conectados al sensor
int ir_raw[8] = {0, 0, 0, 0, 0, 0, 0, 0};           // Mediciones sin procesar del sensor
bool ir[8] = {0, 0, 0, 0, 0, 0, 0, 0};              // Vector de buleanos que indica la posición de la linea

// FUNCIONES
void setup_qtr() {
  // Configuración hardware del sensor QTR-8A
  for (int i = 0; i < 8; i++) {
    pinMode(matriz[i], INPUT);
  }
}

void calibrar_matriz() {
  // Calibración del umbral medio
  int max_intensity = 0;
  int min_intensity = 4000;

  for (int i = 0; i < 8; i++) {
    if ( ir_raw[i] > max_intensity ) max_intensity = ir_raw[i];
    if ( ir_raw[i] < min_intensity ) min_intensity = ir_raw[i];
  }

  average_intensity = (max_intensity + min_intensity) / 2;
}


void pd() {
  // Alogritmo pd para calcular la actuación sobre los motores
  for (int i = 0; i < 8; i++) {
    ir_raw[i] = analogRead(matriz[i]);
    ir_raw[i] = ajusteQTR(i);

    if (ir_raw[i] > average_intensity) ir [i] = 1;
    else                     ir [i] = 0;
  }

  print_QTR(ir_raw, average_intensity);

  // Ponderaciones de la matriz 
  p = - 5 * ir[1] - 3 * ir[2] - ir[3] + ir[4] + 3 * ir[5] + 5 * ir[6] ;
  p = p * 1.5 * (vbase / 239.0);

  // Aplicamos PD  
  d = p - previous_p;
  previous_p = p;
  u = kp * p + kd * d;


  l_speed = constrain(vbase - u, 0, 255);
  r_speed = constrain(vbase + u, 0, 255);

  // aplicamos corrección a los motores
  ledcWrite(0, r_speed); //Motor derecha
  ledcWrite(1, l_speed); //Motor izquierda

  print_siguelineas(p, u, l_speed, r_speed);
}

void line_follow() {
  // Algoritmo siguelineas con llamda a la calibración
  pd();
  if (count_calibrate >= intervalo_calibrar) {
    count_calibrate = 0;
    calibrar_matriz();
  }
  else {
    count_calibrate++;
  }
}

float ajusteQTR(int i) {
  // Calibración manual del sensor QTR-8A
  switch (i) {
    case 0:
      return ir_raw[i] - 170;
      break;
    case 1:
      return ir_raw[i] + 10;
      break;
    case 2:
      return ir_raw[i] + 30;
      break;
    case 3:
      return ir_raw[i] + 30;
      break;
    case 4:
      return ir_raw[i] - 40;
      break;
    case 5:
      return ir_raw[i] + 10;
      break;
    case 6:
      return ir_raw[i] + 100;
      break;
    case 7:
      return ir_raw[i] - 20;
      break;
  }
}


void restart_pd() {
  // Reiniciar algoritmo pd
  previous_p = 0;
  u = 0;
  d = 0;
  count_calibrate = 0;
}

void update_vel(int vel) {
  // Actualizar valocidad de base según consigna de potencia
  vbase = vel;
}
