// BIBLIOTECAS
#include <Wire.h>
#include <INA219.h>

// CONSTANTES
#define SHUNT_MAX_V 0.4   // Voltaje máximo soportado por la resistencia
#define BUS_MAX_V   16.0  // Valores posibles 32V y 16V
#define MAX_CURRENT 5     // Valor aproximado de la corriente máxima
#define SHUNT_R   0.1     // resistencia de shunt en ohms

// VARIABLES
float power = 0;          // Potencia media
float power_times = 0;    // Número de veces que se mide la potencia
int div219 = 0;           // numero de llamadas a la función measure_power
INA219 monitor;           // Objeto INA219

// FUNCIONES
void setup_ina219() {
  // Configurar hardware sensor ina219 y calibrar
  monitor.begin();
  monitor.configure(INA219::RANGE_16V, INA219::GAIN_2_80MV, INA219::ADC_64SAMP, INA219::ADC_64SAMP, INA219::CONT_SH_BUS);
  monitor.calibrate(SHUNT_R, SHUNT_MAX_V, BUS_MAX_V, MAX_CURRENT);
}


void test() {
  // Función de debug para comprobar el correcto funcionamiento del sensor
  power = monitor.busPower() * 1000 ;
  Serial.print("Power: ");
  Serial.print(power);
  Serial.print("mW        -      Current: ");
  Serial.print(monitor.shuntCurrent() * 1000);
  Serial.println("mA");
  delay(50);
}

void meassure_power() {
  // Solo medir potencia cada 4 llamadas, para ahorrar tiempo de procesamiento
  if (div219 == 4) {
    div219 = 0;
    power += monitor.busPower() * 1000 ; // In mW
    power_times++;
    print_ina219(power, power_times);
  } else {
    div219++;
  }
}

float get_power() {
  // Calcular potencia media
  float prov = power / power_times;
  power_times = 0;
  power = 0;
  return prov;
}
