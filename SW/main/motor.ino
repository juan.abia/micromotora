// BIBLIOTECAS
#include "ESP32MotorControl.h"

// CONSTANTES
#define In1 5
#define In2 2
#define In3 19
#define In4 18

// FUNCIONES
void setup_motors() {
  // Configuración hardware de los drivers
  pinMode(In1, OUTPUT);
  pinMode(In2, OUTPUT);
  pinMode(In3, OUTPUT);
  pinMode(In4, OUTPUT);

  digitalWrite(In1, HIGH);
  digitalWrite(In2, LOW);
  digitalWrite(In3, HIGH);
  digitalWrite(In4, LOW);

  // pwm freq 2.5 khz, Resolution 8 bits
  ledcAttachPin (In1, 0);
  ledcSetup(0, 2500, 8); //Canal 0 -> derecha, adelante
  ledcAttachPin (In3, 1);
  ledcSetup(1, 2500, 8); //Canal 1 -> izquierda, adelante
}

void startMotors() {
  // Empezar motores con potencia 200
  ledcWrite(0, 200);
  ledcWrite(1, 200);
}

void stop_motors() {
  // Parar motores
  ledcWrite(0, 0);
  ledcWrite(1, 0);
}

void test_motors() {
  ledcWrite(1, 40);
  ledcWrite(0, 40);
}
