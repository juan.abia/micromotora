// CONSTANTES
const byte DIST_TRIGGER = 4;    // Pin del micro conectado al terminal trigger del sensor
const byte DIST_ECHO = 23;      // Pin del micro conectado al terminal echo del sensor
const int N_SAMPLES = 10;       // Tamaño del vector de distancias
const int DIST_THRESHOLD = 7;   // Distancia donde el robot detecta una baliza

// VARIABLES
int beacon_distance[N_SAMPLES]; // Vector de las ultimas mediciones
float average_distance;         // Distancia media         
unsigned long last_time = 0;    // Ultimo instante en el que se detecto baliza

// FUNCIONES
void setup_ultrasonic() {
  // Configura el hardware del sensor ultrasonidos
  pinMode(DIST_TRIGGER, OUTPUT);
  pinMode(DIST_ECHO, INPUT);
  restart_distance();
}

float readDistanceSensor() {
  // Mide la distancia que detecta el sensor de ultrasonidos
  digitalWrite(DIST_TRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(DIST_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(DIST_TRIGGER, LOW);
  
  float duration = pulseIn(DIST_ECHO, HIGH);  // en microsegundos
  float distance = duration * 0.0343 / 2.0;   // en centimetros

  return distance;
}


int sectionDone() {
  // Devuelve 1 si se detecta una baliza

  rotate_array();
  beacon_distance[N_SAMPLES - 1] = readDistanceSensor();

  average_distance = calculate_average();

  print_distancia(beacon_distance, N_SAMPLES, average_distance);
  
  if (millis() - last_time > 1500) {
    if (average_distance < DIST_THRESHOLD) {
      last_time = millis();
      return true;
    }
  }
  return false;
}

void rotate_array() {
  // Rotar el vector de distancias
  for (int i = 0; i < N_SAMPLES - 1; i++) {
    beacon_distance[i] = beacon_distance[i + 1];
  }
}

float calculate_average() {
  // Calcular la media del vector de distancias
  average_distance = 0;
  for (int i = 0; i < N_SAMPLES; i++) {
    average_distance += beacon_distance[i];
  }
  average_distance /= N_SAMPLES;
  return average_distance;
}

void restart_distance() {
  // Reinicia el vector de distancias con valores elevados
  for (int i = 0; i < N_SAMPLES; i++) {
    beacon_distance[i] = 100;
  }
}

bool stages_end() {
  // Aumenta el numero de tramo y devuelve 1 si es el último
  stage++;
  return stage == 6;
}
