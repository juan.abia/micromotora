/**********************************************
  INA219 library example
  10 May 2012 by johngineer

  9 January 2016 Flavius Bindea: changed default values and begin()

  this code is public domain.
**********************************************/


#include <Wire.h>
#include <INA219.h>

#define SHUNT_MAX_V 0.4  /* Rated max for our shunt is 75mv for 50 A current: 
                             we will mesure only up to 20A so max is about 75mV*20/50 lets put some more*/
#define BUS_MAX_V   16.0  /* with 12v lead acid battery this should be enough*/
#define MAX_CURRENT 5    /* In our case this is enaugh even shunt is capable to 50 A*/
#define SHUNT_R   0.1   /* Shunt resistor in ohm */

INA219 monitor;


void setup() {
  Serial.begin(115200);
  monitor.begin();
  // begin calls:
  // configure() with default values RANGE_32V, GAIN_8_320MV, ADC_12BIT, ADC_12BIT, CONT_SH_BUS
  // calibrate() with default values D_SHUNT=0.1, D_V_BUS_MAX=32, D_V_SHUNT_MAX=0.2, D_I_MAX_EXPECTED=2
  // in order to work directly with ADAFruit's INA219B breakout

  monitor.configure(INA219::RANGE_16V, INA219::GAIN_2_80MV, INA219::ADC_64SAMP, INA219::ADC_64SAMP, INA219::CONT_SH_BUS);

  // calibrate with our values
  monitor.calibrate(SHUNT_R, SHUNT_MAX_V, BUS_MAX_V, MAX_CURRENT);
}

float power = 0;
float media = 20;
void loop() {
  power = monitor.busPower() * 1000 ;
  Serial.print("Power: ");
  Serial.print(power);
  Serial.print("mW        -      Current: ");
  Serial.print(monitor.shuntCurrent() * 1000);
  Serial.println("mA");
  delay(50);

}
