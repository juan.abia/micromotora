// BIBLIOTECAS
#include <WiFi.h>
#include <PubSubClient.h>
#include <soc/sens_reg.h>

// CONSTANTES
const char* ssid = "******";                        // SSID de la wifi
const char* password = "******";                    // Contraseña de la wifi
const char* mqtt_server = "io.adafruit.com";        // Servidor mqtt
const char* mqtt_user = "juan_abia";                // Usuario mqtt
const char* mqtt_key = "******";                    // Clave mqtt
const char* outTopic = "juan_abia/feeds/outTopic";  // Tópico de salida
const char* inTopic = "juan_abia/feeds/inTopic";    // Tópico de entrada

// VARIABLES
int len_power_array = 11;
char character;                           
float potencias[6] = {0, 0, 0, 0, 0, 0};  // Vector de potencias a enviar
float tiempos[6] = {0, 0, 0, 0, 0, 0};    // Vector de tiempos a enviar
uint64_t reg_a;                           // Registro interno modulo wifi
uint64_t reg_b;                           // Registro interno modulo wifi
uint64_t reg_c;                           // Registro interno modulo wifi
WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  // Configuración hardware del modulo wifi
  reg_a = READ_PERI_REG(SENS_SAR_START_FORCE_REG);
  reg_b = READ_PERI_REG(SENS_SAR_READ_CTRL2_REG);
  reg_c = READ_PERI_REG(SENS_SAR_MEAS_START2_REG);

  delay(10);
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conectada");
}

void reconnect() {
  // Reconectar a adafruit server
  while (!client.connected()) {
    Serial.print("Connecting to  ");
    Serial.println(mqtt_server);
    if (client.connect("juan.abia999", mqtt_user, mqtt_key)) {
      Serial.println("");
      Serial.println("Connected to adafruit!");

      client.subscribe(inTopic);
    }
    else {
      Serial.print("-");
      delay(5000);
    }
  }
}



void callback(char* topic, byte* payload, unsigned int length) {
  // Función ejecutada al recibir datos en el topico especificado
  String strArray[len_power_array] = {"", "", "", "", "", "", "", "", "", "", ""};
  char* ptr;

  int j = 0;
  for (int i = 0; i < length; i++) {
    character = (char) payload[i];

    if (character != ',') {
      strArray[j] = strArray[j] + character;
    }
    else if (character == ',') {
      // Serial.println( strtol(strArray[j].c_str(), &ptr, 10));
      j++;
    }
  }
  for (int i = 0; i < len_power_array; i++) {
    power_array[i] = atoi(strArray[i].c_str());
  }
  print_recibido(power_array, len_power_array);
}

void setup_comms() {
  // Configurar comunicación mqtt
  //setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}



void receive_mqtt() {
  // Empezar a recibir mensajes del servidor
  if (!client.connected()) {
    setup_wifi();
    reconnect();
  }

  client.loop();
}

void sendMQTT(float number) {
  // Fucnión debug para enviar numero al servidor
  char out[50] = "time:";
  char out2[50];
  
  if (!client.connected()) {
    setup_wifi();
    reconnect();
  }
  
  sprintf(out2, "%s %f", out, number);  // Create out2 string
  Serial.println(out2);
  client.publish(outTopic, out2);       // Send out2 string
}

void send_times() {
  // Enviar vector de tiempos
  char str_time[50] = "time:";
  char str_time_end[50];
  
  if (!client.connected()) {
    setup_wifi();
    reconnect();
  }
  sprintf(str_time_end, "%s [%f,%f,%f,%f,%f,%f]", str_time, tiempos[0], tiempos[1], tiempos[2], tiempos[3], tiempos[4], tiempos[5]);
  Serial.println(str_time_end);
  client.publish(outTopic, str_time_end);
}

void send_power() {
  // Enviar vector de potencias medias
  char str_power[50] = "power:";
 char str_power_end[50];
 
  if (!client.connected()) {
    setup_wifi();
    reconnect();
  }
  sprintf(str_power_end, "%s [%f,%f,%f,%f,%f,%f]", str_power, potencias[0], potencias[1], potencias[2], potencias[3], potencias[4], potencias[5]);
  Serial.println(str_power_end);
  client.publish(outTopic, str_power_end);
}

void stop_wifi() {
  // Forzar la desconexión de la conexión wifi
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
  WRITE_PERI_REG(SENS_SAR_START_FORCE_REG, reg_a);
  WRITE_PERI_REG(SENS_SAR_READ_CTRL2_REG, reg_b);
  WRITE_PERI_REG(SENS_SAR_MEAS_START2_REG, reg_c);
}

void save_data(float tim, float pot) {
  // Guardar datos de la trama actual
  tiempos[stage] = tim;
  potencias[stage] = pot;
}

void send_data() {
  // Enviar datos
  for (int i = 0; i < 6; i++) {
    sendMQTT(tiempos[i]);
    delay(2000);
    tiempos[i] = 0;

  }
  for (int i = 0; i < 6; i++) {
    sendMQTT(potencias[i]);
    delay(2000);
    potencias[i] = 0;

  }
  stage = 0;
}
