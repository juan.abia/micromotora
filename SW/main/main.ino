// VARIABLES
int phase = 1;
int power_array[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int stage = 0;
unsigned long section_start = 0;
unsigned long section_time = 0;
unsigned long start_sending = 0;

// FUNCIONES
void setup() {
  // Función ejecutada unicamente al encender el microcontrolador
  Serial.begin(115200);
  pinMode(2, OUTPUT);

  setup_comms();
  setup_ultrasonic();
  setup_motors();
  setup_qtr();
  setup_ina219();
  
  delay(1000);
}

void loop() {
  // Función ejecutada en bucle
  switch (phase) {

    // Esperar por vector de potencia
    case 1:     
      receive_mqtt();
      
      if (power_array[0] != 0) {
        power_array[0] = 0;
        stop_wifi();
        phase = 2;
        update_vel(power_array[0]);

        // Comprobar functionamiento motores
        section_start = millis();
        test_motors();
        while(section_start + 1000 > millis());
        stop_motors();
        section_start = millis();
      }

      break;

    // Medir tiempo y potencia hasta la siguiente baliza. Ejecutar algoritmo siguelineas
    case 2:     
      line_follow();

      if (sectionDone() == 1) {
        phase = 3;
        restart_distance();
        Serial.println("Baliza detectada");
        restart_pd();
      }

      meassure_power();

      break;

    // Si completamos el tramo, enviar datos. Si no, continuar
    case 3:     
      section_time = millis() - section_start;
      save_data(float(section_time / 1000.0), get_power());

      if (stages_end()) {
        phase = 4;
      } else {
        phase = 2;
        update_vel(power_array[stage]);
        section_start = millis();
      }

      break;

    // Mandar datos y reiniciar
    case 4:
      stop_motors();
      start_sending = millis();
      send_times();
      while (start_sending + 2500 > millis());
      send_power();
      while (start_sending + 10000 > millis());

      stop_wifi();
      ESP.restart();
      break;

    default:
      Serial.println("ERROR: Case fuera de rango");
      break;
  }
}
