// VARIABLES
bool debug_QTR = 0;
bool debug_siguelineas = 0;
bool debug_mqtt = 0;
bool debug_baliza = 0;
bool debug_fases = 0;
bool debug_distancia = 0;
bool debug_ina219 = 0;

// FUNCIONES
void print_siguelineas(float p, int u, int l, int r) {
  // Imprimir valores debug siguelineas
  if (debug_siguelineas) {
    Serial.print("p:");
    Serial.print(p);
    Serial.print(",u:");
    Serial.print(u);
    Serial.print(",left:");
    Serial.print(l);
    Serial.print(",right:");
    Serial.println(r);
  }
}

void print_ina219(float power, float power_times) {
  // Imprimir valores debug ina219
  if (debug_ina219) {
    Serial.print(power / power_times);
    Serial.print(" - ");
    Serial.println(power_times);
  }
}

void print_fases(int phase) {
  // Imprimir valores debug fases
  if (debug_fases) {
    Serial.print("phase: ");
    Serial.println(phase);
  }
}

void print_QTR(int * ir_raw, int medio) {
  // Imprimir valores debug sensor QTR-8A
  if (debug_QTR) {
    for (int i = 0; i < 8; i++) {
      Serial.print(i);
      Serial.print(":");
      Serial.print(ir_raw[i]);
      Serial.print(",");
    }
    Serial.print("media:");
    Serial.println(medio);
  }
}

void print_distancia(int * distancias, int N_SAMPLES, float distancia) {
  // Imprimir valores debug sensor de distancia
  if (debug_distancia) {
    Serial.print("[");
    for (int i = 0; i < N_SAMPLES; i++) {
      Serial.print(distancias[i]);
      Serial.print(", ");
    }
    Serial.print("]");
    Serial.print("\tDistancia:");
    Serial.println(distancia);
  }
}

void print_recibido(int * power_array, int len_power_array) {
  // Imprimir valores debug sobre vector recibido por MQTT
  if (debug_mqtt) {
    for (int i = 0; i < len_power_array; i++) {
      Serial.print(power_array[i]);
    }
    Serial.println("");
  }
}
