#include <Wire.h>
#include <INA219.h>

#define In1 5
#define In2 2
#define In3 19
#define In4 18

INA219 monitor;

void setup() {
  pinMode(In1, OUTPUT);
  pinMode(In3, OUTPUT);

  ledcAttachPin (In1,0);
  ledcSetup(0,500,8); // channel 0, pwm freq 5 khz, Resolution 8 bits
  ledcAttachPin (In3,1);
  ledcSetup(1,500,8); // channel 0, pwm freq 5 khz, Resolution 8 bits

  Serial.begin(115200);
  monitor.begin();

  delay(2000);
  monitor.configure(INA219::RANGE_16V, INA219::GAIN_2_80MV, INA219::ADC_64SAMP, INA219::ADC_64SAMP, INA219::CONT_SH_BUS);
  ledcWrite(0, 255);
  ledcWrite(1, 255);
  
}

const int tiempo = 4000; // En milisegundos
int tick = millis();
int tack = 0;
int fin = 0;
float power = 0;
int times = 0;

void loop() {


  power += monitor.busPower();
  times++;
  
  /*Función timer*/
  if (fin == 0){
    tack = millis();
    if (tack > (tick + tiempo)){
      fin = 1;
      ledcWrite(0, 0);
      ledcWrite(1, 0);
      power = power/times;
      Serial.print("Average power in ");
      Serial.print(tiempo/1000);
      Serial.print("s: ");
      Serial.print(power);
      Serial.println("W");
      Serial.print("Total consumption: ");
      Serial.print(1000*power * (tiempo/1000)/3600);
      Serial.println("mWh");
     
    }
  }
  delay(20);
}
