/**********************************************
* INA219 library example
* 10 May 2012 by johngineer
*
* 9 January 2016 Flavius Bindea: changed default values and begin()
*
* this code is public domain.
**********************************************/


#include <Wire.h>
#include <INA219.h>

#define SHUNT_MAX_V 0.04  /* Rated max for our shunt is 75mv for 50 A current: 
                             we will mesure only up to 20A so max is about 75mV*20/50 lets put some more*/
#define BUS_MAX_V   5  /* with 12v lead acid battery this should be enough*/
#define MAX_CURRENT 5    /* In our case this is enaugh even shunt is capable to 50 A*/
#define SHUNT_R   0.01   /* Shunt resistor in ohm */

#define In1 5
#define In2 2
#define In3 19
#define In4 18

INA219 monitor;


void setup(){
  pinMode(In1, OUTPUT);
  pinMode(In3, OUTPUT);
    
  ledcAttachPin (In1,0);
  ledcSetup(0,500,8); // channel 0, pwm freq 5 khz, Resolution 8 bits
  ledcAttachPin (In3,1);
  ledcSetup(1,500,8); // channel 0, pwm freq 5 khz, Resolution 8 bits
  
  Serial.begin(115200);
  monitor.begin();
  // begin calls:
  // configure() with default values RANGE_32V, GAIN_8_320MV, ADC_12BIT, ADC_12BIT, CONT_SH_BUS
  // calibrate() with default values D_SHUNT=0.1, D_V_BUS_MAX=32, D_V_SHUNT_MAX=0.2, D_I_MAX_EXPECTED=2
  // in order to work directly with ADAFruit's INA219B breakout

  monitor.configure(INA219::RANGE_16V, INA219::GAIN_2_80MV, INA219::ADC_64SAMP, INA219::ADC_64SAMP, INA219::CONT_SH_BUS);
  
  // calibrate with our values
  monitor.calibrate(SHUNT_R, SHUNT_MAX_V, BUS_MAX_V, MAX_CURRENT);
}

float power = 0;
int media = 20;

void loop(){ 

  ledcWrite(0, 200);
  ledcWrite(1, 200);
  
  power = 0;
  for (int i = 0; i<media; i++){
   power = power + monitor.busPower() * 1000;
   delay(5);
  }
  Serial.println(power/media);
  //Serial.println(fordward);
  delay(10);
}
