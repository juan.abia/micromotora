# Bibliotecas
from Adafruit_IO import Client, Data
from time import sleep
import random
import csv
import os
import matplotlib.pyplot as plt

ADAFRUIT_IO_KEY = "******"          # Clave del servidor mqtt
ADAFRUIT_IO_USER_NAME = "juan_abia" # Usuario del servidor mqtt
IN_TOPIC = "intopic"                # Tópico de entrada
OUT_TOPIC = "outtopic"              # Tópico de salida

# Conexión con servidor mqtt
aio = Client(key=ADAFRUIT_IO_KEY, username=ADAFRUIT_IO_USER_NAME)

# Variables
row = []
velocities = [150, 150, 150, 150, 150, 150] # Vector de velocidades o potencias
pow = ""

# Eliminar datos del servidor para no saturarlo
def delete_feed_data(topic):
    while True:
        try:
            data = aio.receive_previous(topic)
            aio.delete(topic, data.id)
        except Exception as e:
            break
        sleep(2) 

# Enviar velocidades o potencias al robot
def send_velocities(v):
    msg = "{},{},{},{},{},{}".format(v[0],v[1],v[2],v[3],v[4],v[5])
    aio.send_data(IN_TOPIC, msg)
    row.append(msg)

# Calculo de perfil de potencias aleatorio dentro de los límites
def calc_random_velocities():
    max_v = [240, 240, 240, 240, 240, 150]
    min_v = [70, 70, 120, 70, 70, 70]
    for i in range(6):
        velocities[i] = random.randint(min_v[i], max_v[i])

# Recivir datos del servidor mqtt
def receive_values():
    recv = 0

    #
    while recv < 2:
        try:
            data = aio.receive(OUT_TOPIC)
            row.append(data.value)
            recv += 1
            aio.delete(OUT_TOPIC, data.id)
        except Exception as e:
            pass
        # Es necesario realizar una espera para no agotar las peticiones
        # por minuto permitidas
        sleep(1)
    print(row)

# Guardar datos en base de datos
def save_data():
    os.chdir("/home/juan-abia/Git/micromotora")
    with open("db.csv", "a") as f:
        writer = csv.writer(f)
        writer.writerow(row)

# Mostrar una gráfica con los datos obtenidos
def plot_data():
    pow = row[2].split("[")[1][:-2]
    pow = pow.split(",")
    for i in range(len(pow)):
        pow[i] = float(pow[i])  
    
    x = range(6)

    plt.plot(x, pow)
    plt.plot(x, velocities)
    plt.show()


# MAIN

# Abrir base de datos para mostrar por pantalla el número de datos obtenidos
os.chdir("/home/juan-abia/Git/micromotora")
file = open("db.csv", "r")
reader = csv.reader(file)
print(len(list(reader)))

# Calcular perfil de potencias aleatorio y mandarlo al robot
calc_random_velocities()
send_velocities(velocities)

sleep(0.5)
delete_feed_data(IN_TOPIC)

# Manualmente indicar cuando el robot a completado una vuelta
input("receive")
receive_values()

plot_data()

# En caso de que la vuelta se inválida (robot desviado, cable desconectado...)
# indicar manualmente que estos datos no se almacenan
print("_____________---_____________")
print("_____________---_____________")
res = input("Save? (y/n)")
if res != "n":
    save_data()





