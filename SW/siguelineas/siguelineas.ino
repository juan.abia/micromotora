/*Variables matriz IR*/
int matriz[8] = {13, 14, 27, 26, 25, 33, 32, 35};
int ir[8] = {0, 0, 0, 0, 0, 0, 0, 0};

/*DEFINES*/
/******************************************************************/
#define In1 5
#define In2 2
#define In3 19
#define In4 18

/*CONSTANTES*/
const int vbase = 200; //PWM base de los motores
const int kp = 30;            //5
const int kd = 5;             //8
int l_speed, r_speed;         //8

/*VARIABLES PID*/
/******************************************************************/
int u = 0;
float p = 0;
int d = 0;
int previous_p = 0;

/*VARIABLES Clibrado Matriz*/
/******************************************************************/
int color_medio = 890;
int media_max = 0;
int media_min = 4000;

/*******************************************************************/
/*                            Calibrado matriz
  /*******************************************************************/
void CalibrarMatriz() {
  int media_max = 0;
  int media_min = 4000;

  for (int i = 0; i < 8; i++) {
    ir[i] = analogRead(matriz[i]);
    ir[i] = ajusteQTR(i);
    Serial.print(ir[i]);
    Serial.print(",");
  }

  Serial.println("");
  
  for (int i = 0; i < 8; i++)
  {
    if ( ir[i] > media_max ) {
      media_max = ir[i];
    }
    else {
      if ( ir[i] < media_min ) {
        media_min = ir[i];
      }
    }
  }

  color_medio = (media_max + media_min) / 2;
  //Serial.println(color_medio);
}


/*******************************************************************/
/*                                  PID
  /*******************************************************************/
void pid() {
  for (int i = 0; i < 7; i++) {
    ir[i] = analogRead(matriz[i]);
    ir[i] = ajusteQTR(i);
  }

  /* Digitalizar medidas analógicas de la matriz   */
  for (int i = 0; i < 7; i++)
  {
    if (ir[i] > color_medio) {        //Línea negra
      //if (ir[i] < color_medio) {        //Línea blanca
      ir [i] = 1;    //Detecto linea
    } else {
      ir [i] = 0;
    }
  }


  /* Ponderaciones de la matriz */
  p = - 5 * ir[1] - 3 * ir[2] - ir[3] + ir[4] + 3 * ir[5] + 5 * ir[6];
  p = p * 2 * (vbase / 239.0);
  /* Aplicamos PD  */
  d = p - previous_p;
  previous_p = p;
  u = kp * p + kd * d;

  // Escalamos la medida entre 0 y 255 para valores asequibles para el pwm de control de
  // los motores
  l_speed = constrain(vbase - u, 0, 255);
  r_speed = constrain(vbase + u, 0, 255);

  /* aplicamos corrección a los motores    */
  ledcWrite(0, r_speed); //Motor derecha?
  ledcWrite(1, l_speed); //Motor izquierda?
}


float ajusteQTR(int i) {
  switch (i) {
    case 0:
      return ir[i] - 170;
      break;
    case 1:
      return ir[i] + 10;
      break;
    case 2:
      return ir[i] + 30;
      break;
    case 3:
      return ir[i] + 30;
      break;
    case 4:
      return ir[i] - 40;
      break;
    case 5:
      return ir[i] + 10;
      break;
    case 6:
      return ir[i] + 100;
      break;
    case 7:
      return ir[i] - 20;
      break;
  }
}

/*******************************************************************/
/*                                 SET-UP
  /*******************************************************************/
void setup() {

  /* Inicializamos puerto serie indicándole el número de bits por segundo (baudios o baudrate)
    9600 suele ser lo común para arduino, pero también se pueden elegir
    https://www.arduino.cc/reference/en/language/functions/communication/serial/begin/     */
  Serial.begin(115200);

  /* Declaramos las salidas al driver como salidas   */
  pinMode(In1, OUTPUT);
  pinMode(In2, OUTPUT);
  pinMode(In3, OUTPUT);
  pinMode(In4, OUTPUT);

  /* Inicializo que el coche avanze nada más empezar, para q el pid luego ya genera valores */
  digitalWrite(In1, HIGH);
  digitalWrite(In2, LOW);
  digitalWrite(In3, HIGH);
  digitalWrite(In4, LOW);

  ledcAttachPin (In1, 0);
  ledcSetup(0, 2500, 8); // channel 0, pwm freq 5 khz, Resolution 8 bits
  ledcAttachPin (In3, 1);
  ledcSetup(1, 2500, 8); // channel 1, pwm freq 5 khz, Resolution 8 bits

  /* Calibrar la matriz   */
  CalibrarMatriz();
}

/*******************************************************************/
/*                                  LOOP
  /*******************************************************************/
void loop() {
  CalibrarMatriz();
  pid();
  pid();
  pid();
  pid();
  pid();

}
